<?php

namespace Drupal\bootstrap_block_italia\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Extending controller base.
 */
class HelperController extends ControllerBase {

  /**
   * Path cleaner method.
   */
  private function pathCleaner($path = NULL) {
    $explodedPath = explode('/', $path);
    foreach ($explodedPath as $key => $value) {
      if (!strcmp($value, '..')) {
        unset($explodedPath[$key]);
      }
    }

    return implode('/', $explodedPath);
  }

  /**
   * Check translated version method.
   */
  private function checkTranslatedVersion($path = NULL, $depth = NULL) {
    $explodedPath   = explode('/', $_SERVER['REQUEST_URI']);
    $otherLanguages = ['en'];

    if (count(array_intersect($explodedPath, $otherLanguages)) > 0) {
      $path = $this->pathCleaner($path);

      for ($index = 0; $index < $depth; $index++) {
        $path = '../' . $path;
      }
    }

    return $path;
  }

  /**
   * Translate path method.
   *
   * @todo: Fix arguments with default values at the end of the argument list.
   */
  public function translatePath($path = NULL, $depth) {
    return $this->checkTranslatedVersion($path, $depth);
  }

}
