<?php

namespace Drupal\bootstrap_block_italia\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bootstrap_block_italia\Controller\HelperController;

/**
 * Tabs vertical class.
 *
 * @Block(
 *   id = "tabs_vertical",
 *   admin_label = @Translation("Schede verticali"),
 * )
 */
class TabsVertical extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form               = parent::blockForm($form, $form_state);
    $config             = $this->getConfiguration();
    $first_tab_title    = 'Tab 1';
    $second_tab_title   = 'Tab 2';
    $third_tab_title    = 'Tab 3';
    $first_tab_content  = 'Platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras. Dictum sit amet justo donec enim diam vulputate ut. Eu nisl nunc mi ipsum faucibus.';
    $second_tab_content = 'Eget egestas purus viverra accumsan. Diam maecenas ultricies mi eget mauris pharetra et. Etiam dignissim diam quis enim. Eu nisl nunc mi ipsum faucibus.';
    $third_tab_content  = 'Euismod lacinia at quis risus sed vulputate. Scelerisque purus semper eget duis at tellus at urna condimentum. Mattis enim ut tellus elementum sagittis.';
    $first_tab_icon     = 'it-link';
    $second_tab_icon    = 'it-link';
    $third_tab_icon     = 'it-link';

    $form['page_depth'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Livello della pagina (solo nel caso di altre lingue):'),
      '#description'   => $this->t('Ad esempio se /page/servizi, il livello di servizi è 2.'),
      '#default_value' => isset($config['page_depth']) ? $config['page_depth'] : 1,
    ];

    $form['tab_id'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('ID del componente:'),
      '#description'   => $this->t('Inserisci un ID per il componente in modo che non vada in conflitto con un altro componente dello stesso tipo.'),
      '#default_value' => isset($config['tab_id']) ? $config['tab_id'] : '',
      '#required'      => TRUE,
    ];

    $form['tabs_padding'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Padding della sezione tabs:'),
      '#description'   => $this->t('Inserisci un eventuale padding per la sezione tabs in px.<br>Se è pari a 0 non sarà preso in considerazione.<br>Inoltre esso sarà applicato a tutti i bordi.'),
      '#default_value' => isset($config['tabs_padding']) ? $config['tabs_padding'] : 0,
    ];

    $form['tabs_margin'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Margine della sezione tabs:'),
      '#description'   => $this->t('Inserisci un eventuale margin per la sezione tabs in px.<br>Se è pari a 0 non sarà preso in considerazione.<br>Inoltre esso sarà applicato a tutti i bordi.'),
      '#default_value' => isset($config['tabs_margin']) ? $config['tabs_margin'] : 0,
    ];

    $form['first_tab_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo del primo tab:'),
      '#description'   => $this->t('Inserisci un titolo per il primo tab.'),
      '#default_value' => isset($config['first_tab_title']) ? $config['first_tab_title'] : $first_tab_title,
    ];

    $form['first_tab_content'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Testo del primo tab:'),
      '#description'   => $this->t('Specifica il contenuto della prima sezione del tab.'),
      '#default_value' => isset($config['first_tab_content']) ? $config['first_tab_content'] : $first_tab_content,
    ];

    $form['first_tab_active'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Attiva il primo tab.'),
      '#description'   => $this->t('Seleziona per attivare il primo tab (resterà visibile ma inaccessibile).'),
      '#default_value' => isset($config['first_tab_active']) ? $config['first_tab_active'] : TRUE,
    ];

    $form['first_tab_icon'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Icona del primo tab:'),
      '#description'   => $this->t('Inserisci (se prevista) il nome della icona per questo primo tab.<br>Essa deve essere una che mette a disposizione Bootstrap Italia.'),
      '#default_value' => isset($config['first_tab_icon']) ? $config['first_tab_icon'] : $first_tab_icon,
    ];

    $form['second_tab_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo del secondo tab:'),
      '#description'   => $this->t('Inserisci un titolo per il secondo tab.'),
      '#default_value' => isset($config['second_tab_title']) ? $config['second_tab_title'] : $second_tab_title,
    ];

    $form['second_tab_content'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Testo del secondo tab:'),
      '#description'   => $this->t('Specifica il contenuto della seconda sezione del tab.'),
      '#default_value' => isset($config['second_tab_content']) ? $config['second_tab_content'] : $second_tab_content,
    ];

    $form['second_tab_active'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Attiva il secondo tab.'),
      '#description'   => $this->t('Seleziona per attivare il secondo tab (resterà visibile ma inaccessibile).'),
      '#default_value' => isset($config['second_tab_active']) ? $config['second_tab_active'] : TRUE,
    ];

    $form['second_tab_icon'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Icona del secondo tab:'),
      '#description'   => $this->t('Inserisci (se prevista) il nome della icona per questo secondo tab.<br>Essa deve essere una che mette a disposizione Bootstrap Italia.'),
      '#default_value' => isset($config['second_tab_icon']) ? $config['second_tab_icon'] : $second_tab_icon,
    ];

    $form['third_tab_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo del terzo tab:'),
      '#description'   => $this->t('Inserisci un titolo per il terzo tab.'),
      '#default_value' => isset($config['third_tab_title']) ? $config['third_tab_title'] : $third_tab_title,
    ];

    $form['third_tab_content'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Testo del terzo tab:'),
      '#description'   => $this->t('Specifica il contenuto della terza sezione del tab.'),
      '#default_value' => isset($config['third_tab_content']) ? $config['third_tab_content'] : $third_tab_content,
    ];

    $form['third_tab_active'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Attiva il terzo tab.'),
      '#description'   => $this->t('Seleziona per attivare il terzo tab (resterà visibile ma inaccessibile).'),
      '#default_value' => isset($config['third_tab_active']) ? $config['third_tab_active'] : TRUE,
    ];

    $form['third_tab_icon'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Icona del terzo tab:'),
      '#description'   => $this->t('Inserisci (se prevista) il nome della icona per questo terzo tab.<br>Essa deve essere una che mette a disposizione Bootstrap Italia.'),
      '#default_value' => isset($config['third_tab_icon']) ? $config['third_tab_icon'] : $third_tab_icon,
    ];

    $form['tab_background'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Attiva lo sfondo per i tabs.'),
      '#description'   => $this->t('Attiva questa opzione per impostare lo sfondo ai tabs.'),
      '#default_value' => isset($config['tab_background']) ? $config['tab_background'] : TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $config    = $this->getConfiguration();
    $helper    = new HelperController();
    $values    = $form_state->getValues();
    $pageDepth = $values['page_depth'];

    $this->configuration['page_depth']         = $values['page_depth'];
    $this->configuration['tab_id']             = $values['tab_id'];
    $this->configuration['tabs_padding']       = $values['tabs_padding'];
    $this->configuration['tabs_margin']        = $values['tabs_margin'];
    $this->configuration['first_tab_title']    = $values['first_tab_title'];
    $this->configuration['first_tab_active']   = $values['first_tab_active'];
    $this->configuration['second_tab_title']   = $values['second_tab_title'];
    $this->configuration['second_tab_active']  = $values['second_tab_active'];
    $this->configuration['third_tab_title']    = $values['third_tab_title'];
    $this->configuration['third_tab_active']   = $values['third_tab_active'];
    $this->configuration['first_tab_content']  = $values['first_tab_content'];
    $this->configuration['second_tab_content'] = $values['second_tab_content'];
    $this->configuration['third_tab_content']  = $values['third_tab_content'];
    $this->configuration['first_tab_icon']     = $values['first_tab_icon'];
    $this->configuration['second_tab_icon']    = $values['second_tab_icon'];
    $this->configuration['third_tab_icon']     = $values['third_tab_icon'];
    $this->configuration['tab_background']     = $values['tab_background'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme'              => 'tabs_vertical',
      '#tab_id'             => $this->configuration['tab_id'],
      '#tabs_padding'       => $this->configuration['tabs_padding'],
      '#tabs_margin'        => $this->configuration['tabs_margin'],
      '#first_tab_title'    => $this->configuration['first_tab_title'],
      '#first_tab_active'   => $this->configuration['first_tab_active'],
      '#second_tab_title'   => $this->configuration['second_tab_title'],
      '#second_tab_active'  => $this->configuration['second_tab_active'],
      '#third_tab_title'    => $this->configuration['third_tab_title'],
      '#third_tab_active'   => $this->configuration['third_tab_active'],
      '#first_tab_content'  => $this->configuration['first_tab_content'],
      '#second_tab_content' => $this->configuration['second_tab_content'],
      '#third_tab_content'  => $this->configuration['third_tab_content'],
      '#first_tab_icon'     => $this->configuration['first_tab_icon'],
      '#second_tab_icon'    => $this->configuration['second_tab_icon'],
      '#third_tab_icon'     => $this->configuration['third_tab_icon'],
      '#tab_background'     => $this->configuration['tab_background'],
    ];
  }

}
