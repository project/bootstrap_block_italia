<?php

namespace Drupal\bootstrap_block_italia\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bootstrap_block_italia\Controller\HelperController;

/**
 * Action Card class.
 *
 * @Block(
 *   id = "action_card",
 *   admin_label = @Translation("Card grande con bottone"),
 * )
 */
class ActionCard extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form           = parent::blockForm($form, $form_state);
    $config         = $this->getConfiguration();
    $card_title     = 'Morbi fermentum amet';
    $card_content   = 'Platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras. Dictum sit amet justo donec enim diam vulputate ut. Eu nisl nunc mi ipsum faucibus.';
    $card_tag_url   = '#';
    $card_tag       = 'Bootstrap';
    $card_date      = '07/01/2021';
    $card_signature = 'di Mario Linguito';
    $card_button    = 'Action';

    $form['page_depth'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Livello della pagina (solo nel caso di altre lingue):'),
      '#description'   => $this->t('Ad esempio se /page/servizi, il livello di servizi è 2.'),
      '#default_value' => isset($config['page_depth']) ? $config['page_depth'] : 1,
    ];

    $form['card_round'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Rounding della card:'),
      '#description'   => $this->t('Inserisci un eventuale rounding per la card in px.<br>Se è pari a 0 non sarà preso in considerazione.<br>Inoltre esso sarà applicato a tutti i bordi.'),
      '#default_value' => isset($config['card_round']) ? $config['card_round'] : 0,
    ];

    $form['card_padding'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Padding della card:'),
      '#description'   => $this->t('Inserisci un eventuale padding per la card in px.<br>Se è pari a 0 non sarà preso in considerazione.<br>Inoltre esso sarà applicato a tutti i bordi.'),
      '#default_value' => isset($config['card_padding']) ? $config['card_padding'] : 0,
    ];

    $form['card_margin'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Margine della card:'),
      '#description'   => $this->t('Inserisci un eventuale margin per la card in px.<br>Se è pari a 0 non sarà preso in considerazione.<br>Inoltre esso sarà applicato a tutti i bordi.'),
      '#default_value' => isset($config['card_margin']) ? $config['card_margin'] : 0,
    ];

    $form['card_date'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Data da mostrare nella card:'),
      '#description'   => $this->t('Inserisci la data da mostrare nella card.'),
      '#default_value' => isset($config['card_date']) ? $config['card_date'] : $card_date,
    ];

    $form['card_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo della card:'),
      '#description'   => $this->t('Inserisci un titolo per questa card.'),
      '#default_value' => isset($config['card_title']) ? $config['card_title'] : $card_title,
    ];

    $form['card_content'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Testo della card:'),
      '#default_value' => isset($config['card_content']) ? $config['card_content'] : $card_content,
    ];

    $form['card_tag'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo del tag:'),
      '#default_value' => isset($config['card_tag']) ? $config['card_tag'] : $card_tag,
    ];

    $form['card_tag_url'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('URL del tag della card:'),
      '#default_value' => isset($config['card_tag_url']) ? $config['card_tag_url'] : $card_tag_url,
    ];

    $form['card_signature'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Firma della card:'),
      '#default_value' => isset($config['card_signature']) ? $config['card_signature'] : $card_signature,
    ];

    $form['card_button'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Nome del bottone della card:'),
      '#default_value' => isset($config['card_button']) ? $config['card_button'] : $card_button,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $config    = $this->getConfiguration();
    $helper    = new HelperController();
    $values    = $form_state->getValues();
    $pageDepth = $values['page_depth'];

    $this->configuration['page_depth']     = $values['page_depth'];
    $this->configuration['card_date']      = $values['card_date'];
    $this->configuration['card_title']     = $values['card_title'];
    $this->configuration['card_content']   = $values['card_content'];
    $this->configuration['card_padding']   = $values['card_padding'];
    $this->configuration['card_margin']    = $values['card_margin'];
    $this->configuration['card_round']     = $values['card_round'];
    $this->configuration['card_tag']       = $values['card_tag'];
    $this->configuration['card_tag_url']   = $values['card_tag_url'];
    $this->configuration['card_signature'] = $values['card_signature'];
    $this->configuration['card_button']    = $values['card_button'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme'          => 'action_card',
      '#card_date'      => $this->configuration['card_date'],
      '#card_title'     => $this->configuration['card_title'],
      '#card_content'   => $this->configuration['card_content'],
      '#card_padding'   => $this->configuration['card_padding'],
      '#card_margin'    => $this->configuration['card_margin'],
      '#card_round'     => $this->configuration['card_round'],
      '#card_tag'       => $this->configuration['card_tag'],
      '#card_tag_url'   => $this->configuration['card_tag_url'],
      '#card_signature' => $this->configuration['card_signature'],
      '#card_button'    => $this->configuration['card_button'],
    ];
  }

}
