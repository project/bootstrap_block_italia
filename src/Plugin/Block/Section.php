<?php

namespace Drupal\bootstrap_block_italia\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bootstrap_block_italia\Controller\HelperController;

/**
 * Section class.
 *
 * @Block(
 *   id = "section",
 *   admin_label = @Translation("Sezione senza cards"),
 * )
 */
class Section extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form           = parent::blockForm($form, $form_state);
    $config         = $this->getConfiguration();
    $section_title  = 'Morbi fermentum amet';
    $first_section  = 'Platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras. Dictum sit amet justo donec enim diam vulputate ut. Eu nisl nunc mi ipsum faucibus.';
    $second_section = 'Eget egestas purus viverra accumsan. Diam maecenas ultricies mi eget mauris pharetra et. Etiam dignissim diam quis enim. Eu nisl nunc mi ipsum faucibus.';
    $third_section  = 'Euismod lacinia at quis risus sed vulputate. Scelerisque purus semper eget duis at tellus at urna condimentum. Mattis enim ut tellus elementum sagittis.';

    $form['page_depth'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Livello della pagina (solo nel caso di altre lingue):'),
      '#description'   => $this->t('Ad esempio se /page/servizi, il livello di servizi è 2.'),
      '#default_value' => isset($config['page_depth']) ? $config['page_depth'] : 1,
    ];

    $form['section_round'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Rounding della sezione:'),
      '#description'   => $this->t('Inserisci un eventuale rounding per la sezione in px.<br>Se è pari a 0 non sarà preso in considerazione.<br>Inoltre esso sarà applicato a tutti i bordi.'),
      '#default_value' => isset($config['section_round']) ? $config['section_round'] : 0,
    ];

    $form['section_padding'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Padding della sezione:'),
      '#description'   => $this->t('Inserisci un eventuale padding per la sezione in px.<br>Se è pari a 0 non sarà preso in considerazione.<br>Inoltre esso sarà applicato a tutti i bordi.'),
      '#default_value' => isset($config['section_padding']) ? $config['section_padding'] : 0,
    ];

    $form['section_margin'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Margine della sezione:'),
      '#description'   => $this->t('Inserisci un eventuale margin per la sezione in px.<br>Se è pari a 0 non sarà preso in considerazione.<br>Inoltre esso sarà applicato a tutti i bordi.'),
      '#default_value' => isset($config['section_margin']) ? $config['section_margin'] : 0,
    ];

    $form['section_background'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Eventuale link per immagine di background:'),
      '#description'   => $this->t('Una immagine specificata va a sovrascrivere il colore selezionato.'),
      '#default_value' => isset($config['section_background']) ? $config['section_background'] : '',
    ];

    $form['section_color'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Colore di background:'),
      '#description'   => $this->t('Seleziona uno dei colori disponibili per lo sfondo.'),
      '#default_value' => isset($config['section_color']) ? $config['section_color'] : '',
      '#options'       => [
        'no_color'     => 'Nessuno',
        'muted'        => 'Tenue',
        'primary'      => 'Blu',
        'neutral'      => 'Neutrale',
      ],
    ];

    $form['section_text_color'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Inserisci un colore per il testo:'),
      '#description'   => $this->t('Se non viene inserito nessun codice colore, allora verrà utilizzato quello di default.'),
      '#default_value' => isset($config['section_text_color']) ? $config['section_text_color'] : '#000000',
    ];

    $form['section_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo della sezione:'),
      '#description'   => $this->t('Inserisci un titolo per questa sezione.'),
      '#default_value' => isset($config['section_title']) ? $config['section_title'] : $section_title,
    ];

    $form['first_section'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Testo della prima sezione:'),
      '#default_value' => isset($config['first_section']) ? $config['first_section'] : $first_section,
    ];

    $form['second_section'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Testo della seconda sezione:'),
      '#default_value' => isset($config['second_section']) ? $config['second_section'] : $second_section,
    ];

    $form['third_section'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Testo della terza sezione:'),
      '#default_value' => isset($config['third_section']) ? $config['third_section'] : $third_section,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $config    = $this->getConfiguration();
    $helper    = new HelperController();
    $values    = $form_state->getValues();
    $pageDepth = $values['page_depth'];

    $this->configuration['page_depth']         = $values['page_depth'];
    $this->configuration['section_background'] = $helper->translatePath($values['section_background'], $pageDepth);
    $this->configuration['section_color']      = $values['section_color'];
    $this->configuration['section_title']      = $values['section_title'];
    $this->configuration['first_section']      = $values['first_section'];
    $this->configuration['second_section']     = $values['second_section'];
    $this->configuration['third_section']      = $values['third_section'];
    $this->configuration['section_padding']    = $values['section_padding'];
    $this->configuration['section_margin']     = $values['section_margin'];
    $this->configuration['section_round']      = $values['section_round'];
    $this->configuration['section_text_color'] = $values['section_text_color'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme'              => 'section',
      '#section_background' => $this->configuration['section_background'],
      '#section_color'      => $this->configuration['section_color'],
      '#section_title'      => $this->configuration['section_title'],
      '#first_section'      => $this->configuration['first_section'],
      '#second_section'     => $this->configuration['second_section'],
      '#third_section'      => $this->configuration['third_section'],
      '#section_padding'    => $this->configuration['section_padding'],
      '#section_margin'     => $this->configuration['section_margin'],
      '#section_round'      => $this->configuration['section_round'],
      '#section_text_color' => $this->configuration['section_text_color'],
    ];
  }

}
