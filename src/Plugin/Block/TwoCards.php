<?php

namespace Drupal\bootstrap_block_italia\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bootstrap_block_italia\Controller\HelperController;

/**
 * Two card class.
 *
 * @Block(
 *   id = "two_cards",
 *   admin_label = @Translation("Card doppia"),
 * )
 */
class TwoCards extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form                   = parent::blockForm($form, $form_state);
    $config                 = $this->getConfiguration();
    $first_card_title       = 'Morbi fermentum amet';
    $first_card_content     = 'Platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras. Dictum sit amet justo donec enim diam vulputate ut. Eu nisl nunc mi ipsum faucibus.';
    $first_card_link_title  = 'Leggi di più';
    $first_card_link_url    = '#';
    $second_card_title      = 'Morbi fermentum amet';
    $second_card_content    = 'Platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras. Dictum sit amet justo donec enim diam vulputate ut. Eu nisl nunc mi ipsum faucibus.';
    $second_card_link_title = 'Leggi di più';
    $second_card_link_url   = '#';

    $form['page_depth'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Livello della pagina (solo nel caso di altre lingue):'),
      '#description'   => $this->t('Ad esempio se /page/servizi, il livello di servizi è 2.'),
      '#default_value' => isset($config['page_depth']) ? $config['page_depth'] : 1,
    ];

    $form['card_round'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Rounding della card:'),
      '#description'   => $this->t('Inserisci un eventuale rounding per la card in px.<br>Se è pari a 0 non sarà preso in considerazione.<br>Inoltre esso sarà applicato a tutti i bordi.'),
      '#default_value' => isset($config['card_round']) ? $config['card_round'] : 0,
    ];

    $form['card_padding'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Padding della card:'),
      '#description'   => $this->t('Inserisci un eventuale padding per la card in px.<br>Se è pari a 0 non sarà preso in considerazione.<br>Inoltre esso sarà applicato a tutti i bordi.'),
      '#default_value' => isset($config['card_padding']) ? $config['card_padding'] : 0,
    ];

    $form['card_margin'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Margine della card:'),
      '#description'   => $this->t('Inserisci un eventuale margin per la card in px.<br>Se è pari a 0 non sarà preso in considerazione.<br>Inoltre esso sarà applicato a tutti i bordi.'),
      '#default_value' => isset($config['card_margin']) ? $config['card_margin'] : 0,
    ];

    $form['first_card_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo della prima card:'),
      '#description'   => $this->t('Inserisci un titolo per la prima card.'),
      '#default_value' => isset($config['first_card_title']) ? $config['first_card_title'] : $first_card_title,
    ];

    $form['first_card_content'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Testo della prima card:'),
      '#default_value' => isset($config['first_card_content']) ? $config['first_card_content'] : $first_card_content,
    ];

    $form['first_card_link_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo del link per la prima card:'),
      '#default_value' => isset($config['first_card_link_title']) ? $config['first_card_link_title'] : $first_card_link_title,
    ];

    $form['first_card_link_url'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('URL del link per la prima card:'),
      '#default_value' => isset($config['first_card_link_url']) ? $config['first_card_link_url'] : $first_card_link_url,
    ];

    $form['second_card_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo della seconda card:'),
      '#description'   => $this->t('Inserisci un titolo per la seconda card.'),
      '#default_value' => isset($config['second_card_title']) ? $config['second_card_title'] : $second_card_title,
    ];

    $form['second_card_content'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Testo della seconda card:'),
      '#default_value' => isset($config['second_card_content']) ? $config['second_card_content'] : $second_card_content,
    ];

    $form['second_card_link_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo del link per la seconda card:'),
      '#default_value' => isset($config['second_card_link_title']) ? $config['second_card_link_title'] : $second_card_link_title,
    ];

    $form['second_card_link_url'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('URL del link per la seconda card:'),
      '#default_value' => isset($config['second_card_link_url']) ? $config['second_card_link_url'] : $second_card_link_url,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $config    = $this->getConfiguration();
    $helper    = new HelperController();
    $values    = $form_state->getValues();
    $pageDepth = $values['page_depth'];

    $this->configuration['page_depth']             = $values['page_depth'];
    $this->configuration['first_card_title']       = $values['first_card_title'];
    $this->configuration['first_card_content']     = $values['first_card_content'];
    $this->configuration['second_card_title']      = $values['second_card_title'];
    $this->configuration['second_card_content']    = $values['second_card_content'];
    $this->configuration['card_padding']           = $values['card_padding'];
    $this->configuration['card_margin']            = $values['card_margin'];
    $this->configuration['card_round']             = $values['card_round'];
    $this->configuration['first_card_link_title']  = $values['first_card_link_title'];
    $this->configuration['first_card_link_url']    = $values['first_card_link_url'];
    $this->configuration['second_card_link_title'] = $values['second_card_link_title'];
    $this->configuration['second_card_link_url']   = $values['second_card_link_url'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme'                  => 'two_cards',
      '#first_card_title'       => $this->configuration['first_card_title'],
      '#first_card_content'     => $this->configuration['first_card_content'],
      '#second_card_title'      => $this->configuration['second_card_title'],
      '#second_card_content'    => $this->configuration['second_card_content'],
      '#card_padding'           => $this->configuration['card_padding'],
      '#card_margin'            => $this->configuration['card_margin'],
      '#card_round'             => $this->configuration['card_round'],
      '#first_card_link_title'  => $this->configuration['first_card_link_title'],
      '#first_card_link_url'    => $this->configuration['first_card_link_url'],
      '#second_card_link_title' => $this->configuration['second_card_link_title'],
      '#second_card_link_url'   => $this->configuration['second_card_link_url'],
    ];
  }

}
