<?php

namespace Drupal\bootstrap_block_italia\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bootstrap_block_italia\Controller\HelperController;

/**
 * Image card class.
 *
 * @Block(
 *   id = "image_card",
 *   admin_label = @Translation("Card grande con immagine"),
 * )
 */
class ImageCard extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form             = parent::blockForm($form, $form_state);
    $config           = $this->getConfiguration();
    $card_title       = 'Morbi fermentum amet';
    $card_image_url   = 'https://picsum.photos/1280/720?image=811';
    $card_link_title  = 'Leggi di più';
    $card_link_url    = '#';
    $card_image_title = 'Immagine';
    $card_image_alt   = 'Immagine';

    $form['page_depth'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Livello della pagina (solo nel caso di altre lingue):'),
      '#description'   => $this->t('Ad esempio se /page/servizi, il livello di servizi è 2.'),
      '#default_value' => isset($config['page_depth']) ? $config['page_depth'] : 1,
    ];

    $form['card_padding'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Padding della card:'),
      '#description'   => $this->t('Inserisci un eventuale padding per la card in px.<br>Se è pari a 0 non sarà preso in considerazione.<br>Inoltre esso sarà applicato a tutti i bordi.'),
      '#default_value' => isset($config['card_padding']) ? $config['card_padding'] : 0,
    ];

    $form['card_margin'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Margine della card:'),
      '#description'   => $this->t('Inserisci un eventuale margin per la card in px.<br>Se è pari a 0 non sarà preso in considerazione.<br>Inoltre esso sarà applicato a tutti i bordi.'),
      '#default_value' => isset($config['card_margin']) ? $config['card_margin'] : 0,
    ];

    $form['card_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo della card:'),
      '#description'   => $this->t('Inserisci un titolo per questa card.'),
      '#default_value' => isset($config['card_title']) ? $config['card_title'] : $card_title,
    ];

    $form['card_image_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo della immagine:'),
      '#description'   => $this->t('Specifica un titolo per la immagine.'),
      '#default_value' => isset($config['card_image_title']) ? $config['card_image_title'] : $card_image_title,
    ];

    $form['card_image_alt'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Alt della immagine:'),
      '#description'   => $this->t('Specifica un testo alternativo per la immagine.'),
      '#default_value' => isset($config['card_image_alt']) ? $config['card_image_alt'] : $card_image_alt,
    ];

    $form['card_image_url'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Link immagine della card:'),
      '#default_value' => isset($config['card_image_url']) ? $config['card_image_url'] : $card_image_url,
    ];

    $form['card_link_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo del link:'),
      '#default_value' => isset($config['card_link_title']) ? $config['card_link_title'] : $card_link_title,
    ];

    $form['card_link_url'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('URL del link:'),
      '#default_value' => isset($config['card_link_url']) ? $config['card_link_url'] : $card_link_url,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $config    = $this->getConfiguration();
    $helper    = new HelperController();
    $values    = $form_state->getValues();
    $pageDepth = $values['page_depth'];

    $this->configuration['page_depth']       = $values['page_depth'];
    $this->configuration['card_title']       = $values['card_title'];
    $this->configuration['card_image_url']   = $values['card_image_url'];
    $this->configuration['card_padding']     = $values['card_padding'];
    $this->configuration['card_margin']      = $values['card_margin'];
    $this->configuration['card_image_title'] = $values['card_image_title'];
    $this->configuration['card_image_alt']   = $values['card_image_alt'];
    $this->configuration['card_link_title']  = $values['card_link_title'];
    $this->configuration['card_link_url']    = $values['card_link_url'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme'            => 'image_card',
      '#card_title'       => $this->configuration['card_title'],
      '#card_image_url'   => $this->configuration['card_image_url'],
      '#card_padding'     => $this->configuration['card_padding'],
      '#card_margin'      => $this->configuration['card_margin'],
      '#card_image_alt'   => $this->configuration['card_image_alt'],
      '#card_image_title' => $this->configuration['card_image_title'],
      '#card_link_title'  => $this->configuration['card_link_title'],
      '#card_link_url'    => $this->configuration['card_link_url'],
    ];
  }

}
