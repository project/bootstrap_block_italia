<?php

namespace Drupal\bootstrap_block_italia\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bootstrap_block_italia\Controller\HelperController;

/**
 * Hero class.
 *
 * @Block(
 *   id = "hero",
 *   admin_label = @Translation("Hero classica, con sfondo, e con card"),
 * )
 */
class Hero extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form                  = parent::blockForm($form, $form_state);
    $config                = $this->getConfiguration();
    $hero_title            = 'Morbi fermentum amet';
    $hero_category         = 'Category';
    $hero_content          = 'Platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras. Dictum sit amet justo donec enim diam vulputate ut. Eu nisl nunc mi ipsum faucibus.';
    $hero_button_title     = 'Leggi di più';
    $hero_button_url       = '#';
    $hero_background_url   = 'https://animals.sandiegozoo.org/sites/default/files/2016-08/animals_hero_mountains.jpg';
    $hero_background_alt   = 'Background hero';
    $hero_background_title = 'Background hero';
    $hero_card_title       = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit';
    $hero_card_content     = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
    $hero_card_link_title  = 'Leggi di più';

    $form['page_depth'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Livello della pagina (solo nel caso di altre lingue):'),
      '#description'   => $this->t('Ad esempio se /page/servizi, il livello di servizi è 2.'),
      '#default_value' => isset($config['page_depth']) ? $config['page_depth'] : 1,
    ];

    $form['hero_round'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Rounding della hero:'),
      '#description'   => $this->t('Inserisci un eventuale rounding per la hero in px.<br>Se è pari a 0 non sarà preso in considerazione.<br>Inoltre esso sarà applicato a tutti i bordi.'),
      '#default_value' => isset($config['hero_round']) ? $config['hero_round'] : 0,
    ];

    $form['hero_padding'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Padding della hero:'),
      '#description'   => $this->t('Inserisci un eventuale padding per la hero in px.<br>Se è pari a 0 non sarà preso in considerazione.<br>Inoltre esso sarà applicato a tutti i bordi.'),
      '#default_value' => isset($config['hero_padding']) ? $config['hero_padding'] : 0,
    ];

    $form['hero_margin'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Margine della hero:'),
      '#description'   => $this->t('Inserisci un eventuale margin per la hero in px.<br>Se è pari a 0 non sarà preso in considerazione.<br>Inoltre esso sarà applicato a tutti i bordi.'),
      '#default_value' => isset($config['hero_margin']) ? $config['hero_margin'] : 0,
    ];

    $form['hero_category'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Categoria della hero:'),
      '#description'   => $this->t('Inserisci una categoria per questa hero.'),
      '#default_value' => isset($config['hero_category']) ? $config['hero_category'] : $hero_category,
    ];

    $form['hero_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo della hero:'),
      '#description'   => $this->t('Inserisci un titolo per questa hero.'),
      '#default_value' => isset($config['hero_title']) ? $config['hero_title'] : $hero_title,
    ];

    $form['hero_content'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Testo della hero:'),
      '#description'   => $this->t('Specifica il contenuto della hero.'),
      '#default_value' => isset($config['hero_content']) ? $config['hero_content'] : $hero_content,
    ];

    $form['hero_button_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo del bottone:'),
      '#default_value' => isset($config['hero_button_title']) ? $config['hero_button_title'] : $hero_button_title,
    ];

    $form['hero_button_url'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('URL del bottone:'),
      '#default_value' => isset($config['hero_button_url']) ? $config['hero_button_url'] : $hero_button_url,
    ];

    $form['hero_background_url'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('URL della immagine di background:'),
      '#description'   => $this->t('Specifica una eventuale immagine di background, altrimenti sarà utilizzato il colore primario.'),
      '#default_value' => isset($config['hero_background_url']) ? $config['hero_background_url'] : $hero_background_url,
    ];

    $form['hero_background_alt'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Alt della immagine di background:'),
      '#description'   => $this->t('Se viene specificata una immagine di background, allora è conveniente usare un alt.'),
      '#default_value' => isset($config['hero_background_alt']) ? $config['hero_background_alt'] : $hero_background_alt,
    ];

    $form['hero_background_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo della immagine di background:'),
      '#description'   => $this->t('Se viene specificata una immagine di background, allora è conveniente usare un titolo.'),
      '#default_value' => isset($config['hero_background_title']) ? $config['hero_background_title'] : $hero_background_title,
    ];

    $form['hero_card_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo della card nella hero:'),
      '#description'   => $this->t('Inserisci un titolo per una eventuale card da aggiungere al componente.'),
      '#default_value' => isset($config['hero_card_title']) ? $config['hero_card_title'] : $hero_card_title,
    ];

    $form['hero_card_content'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Contenuto della card nella hero:'),
      '#description'   => $this->t('Inserisci un contenuto per una eventuale card da aggiungere al componente.'),
      '#default_value' => isset($config['hero_card_content']) ? $config['hero_card_content'] : $hero_card_content,
    ];

    $form['hero_card_link_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo del link della card nella hero:'),
      '#description'   => $this->t('Inserisci un titolo per il link per una eventuale card da aggiungere al componente.'),
      '#default_value' => isset($config['hero_card_link_title']) ? $config['hero_card_link_title'] : $hero_card_link_title,
    ];

    $form['hero_card_link_url'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('URL del link della card nella hero:'),
      '#description'   => $this->t('Inserisci un URL per il link per una eventuale card da aggiungere al componente.'),
      '#default_value' => isset($config['hero_card_link_url']) ? $config['hero_card_link_url'] : '#',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $config    = $this->getConfiguration();
    $helper    = new HelperController();
    $values    = $form_state->getValues();
    $pageDepth = $values['page_depth'];

    $this->configuration['page_depth']            = $values['page_depth'];
    $this->configuration['hero_title']            = $values['hero_title'];
    $this->configuration['hero_content']          = $values['hero_content'];
    $this->configuration['hero_padding']          = $values['hero_padding'];
    $this->configuration['hero_margin']           = $values['hero_margin'];
    $this->configuration['hero_round']            = $values['hero_round'];
    $this->configuration['hero_button_title']     = $values['hero_button_title'];
    $this->configuration['hero_button_url']       = $values['hero_button_url'];
    $this->configuration['hero_category']         = $values['hero_category'];
    $this->configuration['hero_background_url']   = $values['hero_background_url'];
    $this->configuration['hero_background_alt']   = $values['hero_background_alt'];
    $this->configuration['hero_background_title'] = $values['hero_background_title'];
    $this->configuration['hero_card_title']       = $values['hero_card_title'];
    $this->configuration['hero_card_content']     = $values['hero_card_content'];
    $this->configuration['hero_card_link_title']  = $values['hero_card_link_title'];
    $this->configuration['hero_card_link_url']    = $values['hero_card_link_url'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme'                 => 'hero',
      '#hero_title'            => $this->configuration['hero_title'],
      '#hero_content'          => $this->configuration['hero_content'],
      '#hero_padding'          => $this->configuration['hero_padding'],
      '#hero_margin'           => $this->configuration['hero_margin'],
      '#hero_round'            => $this->configuration['hero_round'],
      '#hero_button_title'     => $this->configuration['hero_button_title'],
      '#hero_button_url'       => $this->configuration['hero_button_url'],
      '#hero_category'         => $this->configuration['hero_category'],
      '#hero_background_url'   => $this->configuration['hero_background_url'],
      '#hero_background_alt'   => $this->configuration['hero_background_alt'],
      '#hero_background_title' => $this->configuration['hero_background_title'],
      '#hero_card_title'       => $this->configuration['hero_card_title'],
      '#hero_card_content'     => $this->configuration['hero_card_content'],
      '#hero_card_link_title'  => $this->configuration['hero_card_link_title'],
      '#hero_card_link_url'    => $this->configuration['hero_card_link_url'],
    ];
  }

}
