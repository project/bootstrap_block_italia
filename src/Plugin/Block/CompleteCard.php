<?php

namespace Drupal\bootstrap_block_italia\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bootstrap_block_italia\Controller\HelperController;

/**
 * Complete Card class.
 *
 * @Block(
 *   id = "complete_card",
 *   admin_label = @Translation("Card grande completa con icona"),
 * )
 */
class CompleteCard extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form            = parent::blockForm($form, $form_state);
    $config          = $this->getConfiguration();
    $card_title      = 'Morbi fermentum amet';
    $card_content    = 'Platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras. Dictum sit amet justo donec enim diam vulputate ut. Eu nisl nunc mi ipsum faucibus.';
    $card_link_title = 'Leggi di più';
    $card_link_url   = '#';
    $card_icon       = 'it-card';
    $card_icon_title = 'Sviluppo';

    $form['page_depth'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Livello della pagina (solo nel caso di altre lingue):'),
      '#description'   => $this->t('Ad esempio se /page/servizi, il livello di servizi è 2.'),
      '#default_value' => isset($config['page_depth']) ? $config['page_depth'] : 1,
    ];

    $form['card_round'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Rounding della card:'),
      '#description'   => $this->t('Inserisci un eventuale rounding per la card in px.<br>Se è pari a 0 non sarà preso in considerazione.<br>Inoltre esso sarà applicato a tutti i bordi.'),
      '#default_value' => isset($config['card_round']) ? $config['card_round'] : 0,
    ];

    $form['card_padding'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Padding della card:'),
      '#description'   => $this->t('Inserisci un eventuale padding per la card in px.<br>Se è pari a 0 non sarà preso in considerazione.<br>Inoltre esso sarà applicato a tutti i bordi.'),
      '#default_value' => isset($config['card_padding']) ? $config['card_padding'] : 0,
    ];

    $form['card_margin'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Margine della card:'),
      '#description'   => $this->t('Inserisci un eventuale margin per la card in px.<br>Se è pari a 0 non sarà preso in considerazione.<br>Inoltre esso sarà applicato a tutti i bordi.'),
      '#default_value' => isset($config['card_margin']) ? $config['card_margin'] : 0,
    ];

    $form['card_icon'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Icona della card:'),
      '#description'   => $this->t('Inserisci il nome della icona per questa card.<br>Essa deve essere una che mette a disposizione Bootstrap Italia.'),
      '#default_value' => isset($config['card_icon']) ? $config['card_icon'] : $card_icon,
    ];

    $form['card_icon_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo della icona della card:'),
      '#description'   => $this->t('Inserisci il titolo della icona per questa card.'),
      '#default_value' => isset($config['card_icon_title']) ? $config['card_icon_title'] : $card_icon_title,
    ];

    $form['card_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo della card:'),
      '#description'   => $this->t('Inserisci un titolo per questa card.'),
      '#default_value' => isset($config['card_title']) ? $config['card_title'] : $card_title,
    ];

    $form['card_content'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Testo della card:'),
      '#default_value' => isset($config['card_content']) ? $config['card_content'] : $card_content,
    ];

    $form['card_link_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Titolo del link:'),
      '#default_value' => isset($config['card_link_title']) ? $config['card_link_title'] : $card_link_title,
    ];

    $form['card_link_url'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('URL del link:'),
      '#default_value' => isset($config['card_link_url']) ? $config['card_link_url'] : $card_link_url,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $config    = $this->getConfiguration();
    $helper    = new HelperController();
    $values    = $form_state->getValues();
    $pageDepth = $values['page_depth'];

    $this->configuration['page_depth']      = $values['page_depth'];
    $this->configuration['card_icon']       = $values['card_icon'];
    $this->configuration['card_icon_title'] = $values['card_icon_title'];
    $this->configuration['card_title']      = $values['card_title'];
    $this->configuration['card_content']    = $values['card_content'];
    $this->configuration['card_padding']    = $values['card_padding'];
    $this->configuration['card_margin']     = $values['card_margin'];
    $this->configuration['card_round']      = $values['card_round'];
    $this->configuration['card_link_title'] = $values['card_link_title'];
    $this->configuration['card_link_url']   = $values['card_link_url'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme'           => 'complete_card',
      '#card_icon'       => $this->configuration['card_icon'],
      '#card_icon_title' => $this->configuration['card_icon_title'],
      '#card_title'      => $this->configuration['card_title'],
      '#card_content'    => $this->configuration['card_content'],
      '#card_padding'    => $this->configuration['card_padding'],
      '#card_margin'     => $this->configuration['card_margin'],
      '#card_round'      => $this->configuration['card_round'],
      '#card_link_title' => $this->configuration['card_link_title'],
      '#card_link_url'   => $this->configuration['card_link_url'],
    ];
  }

}
